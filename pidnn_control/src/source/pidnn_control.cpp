#include <iostream>
#include "pidnn_control.h"

PIDNN::PIDNN() {
    wo << 0.0, 0.0, 0.0;
    wi << -1.0, -1.0, -1.0,
            1.0, 1.0, 1.0;
    Awo << 0.0, 0.0, 0.0;
    Awi << 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0;
	feedback = 0.0;
	reference = 0.0;
	output = 0.0;
	integrator = 0.0;
    last_error = 0.0;
	started = false;
    last_output = 0.0;
    last_feedback = 0.0;
    integrator = 0.0;
    no << 0.2, 0.2, 0.2;
    ni << 0.2, 0.2, 0.2,
            0.2, 0.2, 0.2;
    Ev_last = 0.0;
    del_med = 0.0;
    u << 0.0,
            0.0,
            0.0;
    o << 0.0,
            0.0,
            0.0;
    enableAntiWindup(false, 0.0);
    enableMaxOutput(false, 0.0);
    enableFeedForward(false, 0.0, 0.0);
}

PIDNN::~PIDNN() {
}

float PIDNN::getOutput() {
    float elapsed = timer.getElapsedSeconds();
	timer.restart(started);
	if (!started) {
		started = true;
		return output;
	}
    // Calculate "error"
    Eigen::Matrix<float, 1, 2> x;
    x << feedback, reference;
    u = x * wi;


    //Error

    float error = reference - feedback;

    // Calculate Output

    if (abs(u(0,1))>0.1 && abs(error)>0.05){   // abs(u(1,0)) limits depends on perturbance
            integrator = integrator + u(0,1) * elapsed; //possibly have stationary error
    }
    else{
            integrator = integrator + error * elapsed;
    }

    float derivative = (u(0,2) - last_error) / elapsed;

    last_error = u(0,2);

        // Hidden Layer (P I D)
    o << u(0,0),
        integrator,
        derivative;

    //Output Layer

    output = wo * o;



    if (feedforward_enabled) {
        output += feedforward_value;
    }

    // Anti-windup
    if (antiwindup_enabled && saturation_enabled && wo(0,1) != 0) {
        float outputpresat = output;
        if (output > saturation_max) output = saturation_max;
        if (output < saturation_min) output = saturation_min;
        integrator += kw * (output - outputpresat) / wo(0,1) + u(0,1) * elapsed;

    } else integrator += u(0,1) * elapsed;

    // Saturation
    if (saturation_enabled && !antiwindup_enabled) {
        if (output > saturation_max) output = saturation_max;
        if (output < saturation_min) output = saturation_min;
    }

    // Update Gains

    float Ev = 1/2 * pow(error,2);

    updateGains(Ev,error);

    //update feedback
    last_feedback = feedback;
    last_output = output;


	return output;
}

//! Old one
void PIDNN::enableAntiWindup(bool enable, float w) {

    antiwindup_enabled = enable;
    kw = w;
}

void PIDNN::enableMaxOutput(bool enable, float max) {
    enableMaxOutput( enable, -max, +max);
}

void PIDNN::enableMaxOutput(bool enable, float min, float max) {
    saturation_enabled = enable;
    saturation_min = min;
    saturation_max = max;
}

void PIDNN::enableFeedForward(bool enable, float mass, float fffactor){
    feedforward_enabled = enable;
    feedforward_value = mass*fffactor;
}


void PIDNN::updateGains(float Ev, float err){
    if (Ev != 0){
        float del = (Ev - Ev_last) / Ev;
        if ((del * del_med > 0) && (abs(del_med)>0.1)){
            no = 0.5 * no;
            ni = 0.5 * ni;
        }
        else{
            no = 1.2 * no;
            ni = 1.2 * ni;
        }
        del_med = 0.25 * del + 0.75 * del_med;
    }
    else{
        no = 0.3 * no;
        ni = 0.3 * ni;
    }
    float d;
    if ((output - last_output) != 0){
        d = sgn((feedback - last_feedback)/(output - last_output));
    }
    else{
        d = sgn(feedback - last_feedback);
    }

    // Hidden to Output Update
    Eigen::Matrix3f awo_aux;
    awo_aux << err*d, 0.0, 0.0,
            0.0, err*d, 0.0,
            0.0, 0.0, err*d;
    Eigen::RowVector3f o_t;
    o_t = o.transpose();
    Awo = 0.7 * Awo - 0.3 * no.cwiseProduct(o_t * awo_aux);

    // Input to Hidden Update
    Eigen::Matrix<float, 2, 1> awi_aux;
    awi_aux << err*d*feedback,
            err*d*reference;
    Awi = 0.7 * Awi - 0.3 * ni.cwiseProduct(awi_aux * wo);

    // Saturate Increments
    float i,j;
    for (i=0;i++;i<3){
        if (Awo(i,0) > 0.05) Awo(i,0) = 0.05;
        if (Awo(i,0) < -0.05) Awo(i,0) = -0.05;
    }
    for (i=0;i++;i<2){
        for (j=0;j++;j<3){
            if (Awi(i,j) > 0.05) Awi(i,j) = 0.05;
            if (Awi(i,j) < -0.05) Awi(i,j) = -0.05;
        }
    }

    //Updating
    wo += Awo;
    wi += Awi;

}

void PIDNN::reset(float p, float i, float d) {
    setGains(p,i,d);
    wi << -1.0, -1.0, -1.0,
            1.0, 1.0, 1.0;
    Awo << 0.0, 0.0, 0.0;
    Awi << 0.0, 0.0, 0.0,
            0.0, 0.0, 0.0;
    Ev_last = 0.0;
    del_med = 0.0;
	integrator = 0.0;
	last_error = 0.0;
    last_output = 0.0;
    last_feedback = 0.0;
    Ev_last = 0.0;
    del_med = 0.0;
	started = false;
}

float PIDNN::sgn(float var){
    if (var > 0) return 1;
    if (var < 0) return -1;
    if (var == 0) return 0;
}
