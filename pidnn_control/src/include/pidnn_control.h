#ifndef pidnn_control
#define pidnn_control


/*!*************************************************************************************
 *  \class     PIDNN
 *
 *  \brief     PIDNN
 *
 *  \details   This class defines the controller based on a PID with a
 *             Neural Network to adapt gains  (Cambiar)
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include "Timer.h"
#include <ros/ros.h>
#include <eigen3/Eigen/Dense>


class PIDNN {
private:
	// Input/output

    float reference;
    float feedback;
    float output;

	// Parameters
    Eigen::Matrix<float, 1, 3> wo;
    Eigen::Matrix<float, 2, 3> wi;
    Eigen::Matrix<float, 1, 3> Awo;
    Eigen::Matrix<float, 2, 3> Awi;
    Eigen::Matrix<float, 1, 3> no;
    Eigen::Matrix<float, 2, 3> ni;
    Eigen::RowVector3f u;
    Eigen::Vector3f o;
    bool  antiwindup_enabled, saturation_enabled, feedforward_enabled;
    float kw;
    float saturation_min, saturation_max;
    float feedforward_value;

	// Internal state
    float integrator;
    float last_error;
    float last_output;
    float last_feedback;
    float Ev_last;
    float del_med;
    Timer timer;
    bool started;

public:
    PIDNN();
    ~PIDNN();

    inline void setGains(float p, float i, float d) { wo(0,0) = p; wo(0,1) = i; wo(0,2) = d; }
    inline void getGains(float &p, float &i, float &d) { p =  wo(0,0); i = wo(0,1); d = wo(0,2); }
    inline void setInt(float integ) { integrator = integ; }
    inline void getInt(float &integ) { integ = integrator; }
    void enableMaxOutput(bool enable, float max);
    void enableAntiWindup(bool enable, const float w);
    void enableMaxOutput(bool enable, float min, float max);
    void enableFeedForward(bool enable, float mass, float fffactor);

    inline void setReference(float ref) { reference = ref; }
    inline void setFeedback(float measure) { feedback = measure; }
    float getOutput();

    void reset(float p, float i, float d);
    void updateGains(float Ev, float err);
    float sgn(float var);
};
#endif /* pidnn_control */
