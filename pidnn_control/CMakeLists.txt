cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME pidnn_control)
project(${PROJECT_NAME})


### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)
add_definitions(-g)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)
#SET(CMAKE_BUILD_TYPE Release) # Release, RelWithDebInfo


set(PIDNN_CONTROL_SOURCE_DIR
        src/source)

set(PIDNN_CONTROL_INCLUDE_DIR
        src/include)

set(PIDNN_CONTROL_HEADER_FILES
        ${PIDNN_CONTROL_INCLUDE_DIR}/pidnn_control.h)

set(PIDNN_CONTROL_SOURCE_FILES
        ${PIDNN_CONTROL_SOURCE_DIR}/pidnn_control.cpp)


find_package(catkin REQUIRED            
                COMPONENTS roscpp pugixml lib_cvgutils)


find_package(Eigen3)
if(NOT EIGEN3_FOUND)
      # Fallback to cmake_modules
      find_package(cmake_modules REQUIRED)
      find_package(Eigen REQUIRED)
      set(EIGEN3_INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS})
      set(EIGEN3_LIBRARIES ${EIGEN_LIBRARIES})  # Not strictly necessary as Eigen is head only
      # Possibly map additional variables to the EIGEN3_ prefix.
      message(WARN "Using Eigen2!")
else()
      set(EIGEN3_INCLUDE_DIRS ${EIGEN3_INCLUDE_DIR})
endif()


catkin_package(
	INCLUDE_DIRS ${PIDNN_CONTROL_INCLUDE_DIR}
        LIBRARIES pidnn_control
        CATKIN_DEPENDS roscpp pugixml lib_cvgutils
  )



include_directories(${PIDNN_CONTROL_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})
include_directories(${EIGEN3_INCLUDE_DIRS})



add_library(pidnn_control ${PIDNN_CONTROL_SOURCE_FILES} ${PIDNN_CONTROL_HEADER_FILES})
add_dependencies(pidnn_control ${catkin_EXPORTED_TARGETS})
target_link_libraries(pidnn_control ${catkin_LIBRARIES})
target_link_libraries(pidnn_control ${EIGEN3_LIBRARIES})

